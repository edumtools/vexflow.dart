@JS('Vex.Flow')
library vexflow.impl;

import "package:func/func.dart";
import "package:js/js.dart";
import 'dart:html' as html;
import 'dart:web_audio' show AudioContext;
import 'dart:typed_data';

@JS()
external String sanitizeDuration(String duration ) ;
@JS()
external Object Inherit(Object child , Object parent , Object object ) ;

@JS()
class RuntimeError   {
	external factory RuntimeError ([ String code, String message ]);
}

@JS()
class RERR   {
	external factory RERR ([ String code, String message ]);
}

@JS()
class CanvasRenderingContext2D extends IRenderContext {
	external factory CanvasRenderingContext2D();
}

@JS('Element')
class ElementBase {
	external factory ElementBase();
	external getAttribute(String name);
	external setAttribute(String name, dynamic value);
	external setStyle(dynamic style);
}

@JS()
class IRenderContext   {
	external factory IRenderContext();
	external void clear();external
	IRenderContext setFont(String family , num size , num weight );external
	IRenderContext setRawFont(String font );external
	IRenderContext setFillStyle(String style );external
	IRenderContext setBackgroundFillStyle(String style );external
	IRenderContext setStrokeStyle(String style );external
	IRenderContext setShadowColor(String color );external
	IRenderContext setShadowBlur(String blur );external
	IRenderContext setLineWidth(num width );external
	IRenderContext setLineCap(String cap_type );external
	IRenderContext setLineDash(String dash );external
	IRenderContext scale(num x , num y );external
	IRenderContext resize(num width , num height );external
	IRenderContext fillRect(num x , num y , num width , num height );external
	IRenderContext clearRect(num x , num y , num width , num height );external
	IRenderContext beginPath();external
	IRenderContext moveTo(num x , num y );external
	IRenderContext lineTo(num x , num y );external
	IRenderContext bezierCurveToTo(num x1 , num y1 , num x2 , num y2 , num x , num y );external
	void quadraticCurveToTo(num x1 , num y1 , num x2 , num y2 );external
	IRenderContext arc(num x , num y , num radius , num startAngle , num endAngle , bool antiClockwise );external
	IRenderContext glow();external
	IRenderContext fill();external
	IRenderContext stroke();external
	IRenderContext closePath();external
	IRenderContext fillText(String text , num x , num y );external
	IRenderContext save();external
	IRenderContext restore();external
	dynamic measureText(String text );
}

@JS()
class IFont   {
	external factory IFont();
	external List<dynamic> get glyphs;
	external set glyphs(List<dynamic> v);
	external String get cssFontWeight;
	external set cssFontWeight(String v);
	external num get ascender;
	external set ascender(num v);
	external num get underlinePosition;
	external set underlinePosition(num v);
	external String get cssFontStyle;
	external set cssFontStyle(String v);
	external dynamic get boundingBox;
	external set boundingBox(dynamic v);
	external num get resolution;
	external set resolution(num v);
	external num get descender;
	external set descender(num v);
	external String get familyName;
	external set familyName(String v);
	external num get lineHeight;
	external set lineHeight(num v);
	external num get underlineThickness;
	external set underlineThickness(num v);
	external dynamic get original_font_information;
	external set original_font_information(dynamic v);
}

@JS()
class Flow {
	num RESOLUTION ;
	num STEM_WIDTH ;
	num STEM_HEIGHT ;
	num STAVE_LINE_THICKNESS ;
	dynamic TIME4_4;
	dynamic unicode;
	dynamic clefProperties(String clef );
	dynamic keyProperties(String key , String clef , dynamic params );
	String integerToNote(num integer ) ;
	dynamic tabToGlyph(String fret );
	num textWidth(String text ) ;
	dynamic articulationCodes(String artic );
	dynamic accidentalCodes(String acc );
	dynamic ornamentCodes(String acc );
	List<dynamic> keySignature(String spec );
	dynamic parseNoteDurationString(String durationString );
	dynamic parseNoteData(dynamic noteData );
	Fraction durationToFraction(String duration ) ;
	num durationToNumber(String duration ) ;
	num durationToTicks(String duration ) ;
	dynamic durationToGlyph(String duration , String type );
	void renderGlyph(IRenderContext ctx , num x_pos , num y_pos , num point , String val , bool nocache ) ;
	List<dynamic> Font;
	String cssFontWeight ;
	num ascender ;
	num underlinePosition ;
	String cssFontStyle ;
	dynamic boundingBox;
	num resolution ;
	num descender ;
	String familyName ;
	num lineHeight ;
	num underlineThickness ;
	dynamic original_font_information;
}

@JS()
class Accidental  extends Modifier  {
	String CATEGORY;
	external factory Accidental ([ String type ]);
	external Modifier setNote(Note note );
	external static bool get DEBUG;
	external static void format(List<Accidental> accidentals , dynamic state );
	external Accidental setAsCautionary();external
	void draw();
	external static void applyAccidentals(List<Voice> voices , String keySignature );
}

@JS()
class Annotation  extends Modifier  {
	String CATEGORY ;
	external factory Annotation ([ String text ]);
	external static bool get DEBUG;
	external static bool format(List<Annotation> annotations , dynamic state );external
	Annotation setTextLine(num line );external
	Annotation setFont(String family , num size , String weight );external
	Annotation setVerticalJustification(AnnotationVerticalJustify just);
	AnnotationJustify getJustification();external
	Annotation setJustification(AnnotationJustify justification);external
	void draw();
}

class AnnotationJustify {
	static const int LEFT = 1;
	static const int CENTER = 2;
	static const int RIGHT = 3;
	static const int CENTER_STEM = 4;
}

class AnnotationVerticalJustify {
	static const int TOP = 1;
	static const int CENTER =2;
	static const int BOTTOM = 3;
	static const int CENTER_STEM = 4;
}

@JS()
class Articulation  extends Modifier  {
	String CATEGORY;
	external factory Articulation ([ String type ]);
	external static bool get DEBUG;
	external static bool format(List<Articulation> articulations , dynamic state );external
	void draw();
}

@JS()
class BarNote  extends Note  {
	external factory BarNote();
	external static bool get DEBUG;
	Barline getType();external
	BarNote setType(Barline type);external
	BoundingBox getBoundingBox();external
	BarNote addToModifierContext();external
	BarNote preFormat();external
	void draw();
}

	class BarlineType {
    	static const int SINGLE = 1;
			static const int DOUBLE = 2;
			static const int END = 3;
			static const int REPEAT_BEGIN = 4;
			static const int REPEAT_END = 5;
			static const int REPEAT_BOTH = 6;
			static const int NONE = 7;
  }

@JS()
class Barline  extends StaveModifier  {
	external factory Barline ([ Barline.type type, num x ]);
	external String getCategory();external
	Barline setX(num x );external
	void draw(Stave stave , num x_shift );external
	void drawVerticalBar(Stave stave , num x , bool double_bar );external
	void drawVerticalEndBar(Stave stave , num x );external
	void drawRepeatBar(Stave stave , num x , bool begin );
}

@JS()
class Beam   {
	external factory Beam ([ List<StemmableNote> notes, bool auto_stem ]);
	external Beam setContext(IRenderContext context );external
	List<StemmableNote> getNotes();external
	num getBeamCount();external
	Beam breakSecondaryAt(List<num> indices );external
	num getSlopeY();external
	void calculateSlope();external
	void applyStemExtensions();external
	List<dynamic> getBeamLines(String duration );external
	void drawStems();external
	void drawBeamLines();external
	Beam preFormat();external
	Beam postFormat();external
	bool draw();external
	num calculateStemDirection(Note notes );
	external static List<Fraction> getDefaultBeamGroups(String time_sig );
	external static List<Beam> applyAndGetBeams(Voice voice , num stem_direction , List<Fraction> groups );
	external static List<Beam> generateBeams(List<StemmableNote> notes , dynamic config );
}

@JS()
class Bend  extends Modifier  {
	String CATEGORY ;
	external factory Bend ([ String text, bool release, List<dynamic> phrase ]);
	external static num get UP;
	external static num get DOWN;
	external static bool format(List<Bend> bends , dynamic state );external
	void setXShift(num value );external
	Bend setFont(String font );external
	String getText();external
	Bend updateWidth();external
	void draw();
}

@JS()
class BoundingBox   {
	external factory BoundingBox ([ num x, num y, num w, num h ]);
	external static BoundingBox copy(BoundingBox that );external
	num getX();external
	num getY();external
	num getW();external
	num getH();external
	BoundingBox setX(num x );external
	BoundingBox setY(num y );external
	BoundingBox setW(num w );external
	BoundingBox setH(num h );external
	void move(num x , num y );external
	BoundingBox clone();external
	BoundingBox mergeWith(BoundingBox boundingBox , IRenderContext ctx );external
	void draw(IRenderContext ctx , num x , num y );
}

@JS()
class CanvasContext    {
	external factory CanvasContext ([ CanvasRenderingContext2D context ]);
	external CanvasContext setLineDash(String dash );external
	CanvasContext scale(num x , num y );external
	CanvasContext resize(num width , num height );external
	CanvasContext fillRect(num x , num y , num width , num height );external
	CanvasContext clearRect(num x , num y , num width , num height );external
	CanvasContext beginPath();external
	CanvasContext moveTo(num x , num y );external
	CanvasContext lineTo(num x , num y );external
	CanvasContext bezierCurveToTo(num x1 , num y1 , num x2 , num y2 , num x , num y );external
	CanvasContext quadraticCurveToTo(num x1 , num y1 , num x2 , num y2 );external
	CanvasContext arc(num x , num y , num radius , num startAngle , num endAngle , bool antiClockwise );external
	CanvasContext glow();external
	CanvasContext fill();external
	CanvasContext stroke();external
	CanvasContext closePath();external
	CanvasContext fillText(String text , num x , num y );external
	CanvasContext save();external
	CanvasContext restore();
	external static num get WIDTH;
	external static num get HEIGHT;external
	void clear();external
	CanvasContext setFont(String family , num size , num weight );external
	CanvasContext setRawFont(String font );external
	CanvasContext setFillStyle(String style );external
	CanvasContext setBackgroundFillStyle(String style );external
	CanvasContext setStrokeStyle(String style );external
	CanvasContext setShadowColor(String style );external
	CanvasContext setShadowBlur(String blur );external
	CanvasContext setLineWidth(num width );external
	CanvasContext setLineCap(String cap_type );
	external TextMetrics measureText(String text );
}

@JS()
class Clef  extends StaveModifier  {
	external factory Clef ([ String clef, String size, String annotation ]);
	external void addModifier();external
	void addEndModifier();
	external static bool get DEBUG;
}

@JS()
class ClefNote  extends Note  {
	external factory ClefNote ([ String clef, String size, String annotation ]);
	external Note setStave(Stave stave );external
	ClefNote setClef(String clef , String size , String annotation );external
	String getClef();external
	BoundingBox getBoundingBox();external
	String getCategory();external
	ClefNote preFormat();external
	void draw();
}

@JS()
class Crescendo  extends Note  {
	external factory Crescendo ([ dynamic note_struct ]);
	external static bool get DEBUG;external
	Crescendo setLine(num line );external
	Crescendo setHeight(num height );external
	Crescendo setDecrescendo(bool decresc );external
	Crescendo preFormat();external
	void draw();
}

@JS()
class  Curve {
   external Curve(Note from , Note to , options);
   static bool DEBUG ;
   external Curve setContext(IRenderContext context ) ;
   external Curve setNotes(Note from , Note to ) ;
   external bool isPartial() ;
   external void renderCurve(dynamic params ) ;
   external bool draw() ;
}

class Position {
	static const int NEAR_HEAD = 1;
	static const int NEAR_TOP = 2;
}

class CurveType {
	 int position;
}

@JS()
class Dot  extends Modifier  {
	String CATEGORY ;
	external factory Dot();
	external Dot setNote(Note note );
  external static void format(num dots , dynamic state );external
  Dot setDotShiftY(num y );external
  void draw();
  external int getPosition();
}

@JS()
@anonymous
class FormatParams {
	external bool get align_rests;
	external factory FormatParams({bool align_rests});
}

@JS()
@anonymous
class TickContextsWrapper {
	external List get array;
	external List get list;
	external Map get map;
	external int get resolutionMultiplier;
}

@JS()
class Formatter   {
	external factory Formatter();
	external static bool get DEBUG;
	external static BoundingBox FormatAndDraw(IRenderContext ctx , Stave stave , List<Note> notes , FormatParams params );
	external static BoundingBox FormatAndDraw2(IRenderContext ctx , Stave stave , List<Note> notes , bool params );
	external static void FormatAndDrawTab(IRenderContext ctx , TabStave tabstave , Stave stave , List<TabNote> tabnotes , List<Note> notes , bool autobeam , dynamic params );
	external static void FormatAndDrawTab2(IRenderContext ctx , TabStave tabstave , Stave stave , List<TabNote> tabnotes , List<Note> notes , bool autobeam , bool params );
	external static Formatter AlignRestsToNotes(List<Note> notes , bool align_all_notes , bool align_tuplets );external
	void alignRests(List<Voice> voices , bool align_all_notes );external
	num preCalculateMinTotalWidth(List<Voice> voices );external
	num getMinTotalWidth();external
	List<ModifierContext> createModifierContexts(List<Voice> voices );external
	TickContextsWrapper createTickContexts(List<Voice> voices );external
	void preFormat(num justifyWidth , IRenderContext rendering_context , List<Voice> voices , Stave stave );external
	Formatter postFormat();external
	Formatter joinVoices(List<Voice> voices );external
	Formatter format(List<Voice> voices , num justifyWidth , dynamic options );external
	Formatter formatToStave(List<Voice> voices , Stave stave , dynamic options );
	external static Formatter SimpleFormat(notes, x);
	}

@JS()
class Fraction   {
	external factory Fraction ([ num numerator, num denominator ]);
	external static num GCD(num a , num b );
	external static num LCM(num a , num b );
	external static num LCMM(num a , num b );external
	Fraction set(num numerator , num denominator );external
	num value();external
	Fraction simplify();external
	Fraction add(Fraction param1 , Fraction param2 );external
	Fraction add2(num param1 , num param2 );external
	Fraction subtract(Fraction param1 , Fraction param2 );external
	Fraction subtract2(num param1 , num param2 );external
	Fraction multiply(Fraction param1 , Fraction param2 );external
	Fraction multiply2(num param1 , num param2 );external
	Fraction divide(Fraction param1 , Fraction param2 );external
	Fraction divide2(num param1 , num param2 );external
	bool equals(Fraction compare );external
	bool greaterThan(Fraction compare );external
	bool greaterThanEquals(Fraction compare );external
	bool lessThan(Fraction compare );external
	bool lessThanEquals(Fraction compare );external
	Fraction clone();external
	Fraction copy(Fraction copy );external
	num quotient();external
	num fraction();external
	Fraction abs();external
	String toString();external
	String toSimplifiedString();external
	String toMixedString();external
	Fraction parse(String str );
}

@JS()
class FretHandFinger  extends Modifier  {
	String CATEGORY ;
	external factory FretHandFinger ([ num num ]);
	external static void format(List<FretHandFinger> nums , dynamic state );external
	Note getNote();external
	FretHandFinger setNote(Note note );external
	num getIndex();external
	FretHandFinger setIndex(num index );
	external int getPosition();external
	FretHandFinger setPosition(position);external
	FretHandFinger setFretHandFinger(num num );external
	FretHandFinger setOffsetX(num x );external
	FretHandFinger setOffsetY(num y );external
	void draw();
}

@JS()
class GhostNote  extends StemmableNote  {
	external factory GhostNote ([ dynamic note_struct ]);
	external Note setStave(Stave stave );external
	bool isRest();external
	GhostNote addToModifierContext(el);external
	GhostNote preFormat();external
	void draw();
}

@JS()
class Glyph   {
	external factory Glyph ([ String code, num point, dynamic options ]);
	external void setOptions(dynamic options );external
	Glyph setStave(Stave stave );external
	Glyph setXShift(num x_shift );external
	Glyph setYShift(num y_shift );external
	Glyph setContext(IRenderContext context );external
	IRenderContext getContext();external
	void reset();external
	Glyph setWidth(num width );external
	dynamic getMetrics();external
	void render(IRenderContext ctx , num x_pos , num y_pos );external
	void renderToStave(num x );
	external static dynamic loadMetrics(IFont font , String code , bool cache );
	external static void renderOutline(IRenderContext ctx , List<num> outline , num scale , num x_pos , num y_pos );
}

@JS()
class GraceNote  extends StaveNote  {
	external factory GraceNote ([ dynamic note_struct ]);
	external num getStemExtension();external
	String getCategory();external
	void draw();
}

@JS()
class GraceNoteGroup  extends Modifier  {
	String CATEGORY ;
	external factory GraceNoteGroup ([ List<GraceNote> grace_notes, bool show_slur ]);
	external Modifier setWidth(num width );external
	Modifier setNote(StaveNote note );
	external static bool get DEBUG;
	external static bool format(List<GraceNoteGroup> gracenote_groups , dynamic state );external
	void preFormat();external
	GraceNoteGroup beamNotes();external
	num getWidth();external
	void setXShift(num x_shift );external
	void draw();
  external int getPosition();
}

@JS()
class KeyManager   {
	external factory KeyManager ([ String key ]);
	external KeyManager setKey(String key );external
	String getKey();external
	KeyManager reset();external
	dynamic getAccidental(String key );external
	dynamic selectNote(String note );
}

@JS()
class KeySignature  extends StaveModifier  {
	external factory KeySignature ([ String key_spec ]);
	external void addModifier();external
	void addAccToStave(Stave stave , dynamic acc , dynamic next );external
	KeySignature cancelKey(String spec );external
	KeySignature addToStave(Stave stave , bool firstGlyph );external
	void convertAccLines(String clef , String type );
}

@JS()
class KeySignatureNote  extends Note  {
	external factory KeySignatureNote([ String key_spec, String clef ]);
	BoundingBox getBoundingBox();external
	String getCategory();external
	ClefNote preFormat();external
	setClef(String clef);external
	void draw();
}

@JS()
class Modifier   {
	String CATEGORY;

	external factory Modifier();
	external static bool get DEBUG;external
	 String getCategory();external
	 num getWidth();external
	 Modifier setWidth(num width );external
	 Note getNote();external
	 Modifier setNote(Note note );external
	 num getIndex();external
	 Modifier setIndex(num index );external
	 IRenderContext getContext();external
	 Modifier setContext(IRenderContext context );external
	 ModifierContext getModifierContext();external
	 Modifier setModifierContext(ModifierContext c );
	 external int getPosition();external
	 Modifier setPosition(int position);external
	 Modifier setTextLine(num line );external
	 Modifier setYShift(num y );external
	 void setXShift(num x );external
	 void draw();
}

class ModifierPosition {
	static const int LEFT = 1;
	static const int RIGHT = 2;
	static const int ABOVE = 3;
	static const int BELOW = 4;
}

@JS()
class ModifierContext   {
	external factory ModifierContext();
	external static bool get DEBUG;external
	ModifierContext addModifier(Modifier modifier );external
	List<Modifier> getModifiers(String type );external
	num getWidth();external
	num getExtraLeftPx();external
	num getExtraRightPx();external
	dynamic getState();external
	dynamic getMetrics();external
	void preFormat();external
	void postFormat();
}

@JS()
class Music   {
	num NUM_TONES ;
	List<String> roots ;
	List<num> root_values ;
	dynamic root_indices;
	List<String> canonical_notes ;
	List<String> diatonic_intervals ;
	dynamic diatonic_accidentals;
	dynamic intervals;
	dynamic scales;
	List<String> accidentals ;
	dynamic noteValues;

	external factory Music();
	external bool isValidNoteValue(num note );external
	bool isValidIntervalValue(num interval );external
	dynamic getNoteParts(String noteString );external
	dynamic getKeyParts(String noteString );external
	num getNoteValue(String noteString );external
	num getIntervalValue(String intervalString );external
	String getCanonicalNoteName(num noteValue );external
	String getCanonicalIntervalName(num intervalValue );external
	num getRelativeNoteValue(num noteValue , num intervalValue , num direction );external
	String getRelativeNoteName(String root , num noteValue );external
	num getScaleTones(String key , List<num> intervals );external
	num getIntervalBetween(num note1 , num note2 , num direction );external
	dynamic createScaleMap(String keySignature );
}

@JS()
class Note extends Tickable {
	String CATEGORY ;
	external factory Note ([ dynamic note_struct ]);
	external Fraction getTicks();external
	num getCenterXShift();external
	bool isCenterAligned();external
	Note setCenterAlignment(bool align_center );external
	Tuplet getTuplet();external
	Note setTuplet(Tuplet tuplet );external
	void addToModifierContext(ModifierContext mc );external
	void preFormat();external
	Note postFormat();external
	Fraction getIntrinsicTicks();external
	void setIntrinsicTicks(Fraction intrinsicTicks );external
	Fraction getTickMultiplier();external
	void applyTickMultiplier(num numerator , num denominator );external
	void setDuration(Fraction duration );external
	dynamic getPlayNote();external
	Note setPlayNote(dynamic note );external
	bool isRest();external
	Note addStroke(num index , Stroke stroke );external
	Stave getStave();external
	Note setStave(Stave stave );external
	String getCategory();external
	Note setContext(IRenderContext context );external
	num getExtraLeftPx();external
	num getExtraRightPx();external
	Note setExtraLeftPx(num x );external
	Note setExtraRightPx(num x );external
	bool shouldIgnoreTicks();external
	num getLineNumber();external
	num getLineForRest();external
	Glyph getGlyph();external
	Note setYs(List<num> ys );external
	List<num> getYs();external
	num getYForTopText(num text_line );external
	BoundingBox getBoundingBox();external
	Voice getVoice();external
	Note setVoice(Voice voice );external
	TickContext getTickContext();external
	Note setTickContext(TickContext tc );external
	String getDuration();external
	bool isDotted();external
	bool hasStem();external
	num getDots();external
	String getNoteType();external
	Note setBeam();external
	Note setModifierContext(ModifierContext mc );external
	Note addModifier(Modifier modifier , num index );external
	dynamic getModifierStartXY();external
	dynamic getMetrics();external
	void setWidth(num width );external
	num getWidth();external
	Note setXShift(num x );external
	num getX();external
	num getAbsoluteX();external
	void setPreFormatted(bool value );
}

@JS()
class NoteHead  extends Note  {
	external factory NoteHead ([ dynamic head_options ]);
	external static bool get DEBUG;external
	String getCategory();external
	NoteHead setContext(IRenderContext context );external
	num getWidth();external
	bool isDisplaced();external
	dynamic getStyle();external
	NoteHead setStyle(dynamic style );external
	Glyph getGlyph();external
	NoteHead setX(num x );external
	num getY();external
	NoteHead setY(num y );external
	num getLine();external
	NoteHead setLine(num line );external
	num getAbsoluteX();external
	BoundingBox getBoundingBox();external
	NoteHead applyStyle(IRenderContext context );external
	NoteHead setStave(Stave stave );external
	NoteHead preFormat();external
	void draw();
}

@JS()
class Ornament  extends Modifier  {
	String CATEGORY ;
	external factory Ornament ([ String type ]);

	external static bool get DEBUG;
	external static bool format(List<Ornament> ornaments , dynamic state );external
	Ornament setDelayed(bool delayed );external
	Ornament setUpperAccidental(String acc );external
	Ornament setLowerAccidental(String acc );external
	void draw();
}


@JS()
class PedalMarking   {
	dynamic GLYPHS;
	external factory PedalMarking ([ List<Note> notes ]);
	external static bool get DEBUG;
	external static PedalMarking createSustain(List<Note> notes );
	external static PedalMarking createSostenuto(List<Note> notes );
	external static PedalMarking createUnaCorda(List<Note> notes );external
	PedalMarking setCustomText(String depress , String release );external
	PedalMarking setStyle(style);external
	PedalMarking setLine(num line );external
	PedalMarking setContext(IRenderContext context );external
	void drawBracketed();external
	void drawText();external
	void draw();

}

class PedalMarkingStyles {
    static const int TEXT = 1;
		static const int BRACKET = 2;
		static const int MIXED = 3;
}

@JS()
class RaphaelContext    {
	external factory RaphaelContext ([ html.Element element ]);
	external RaphaelContext setLineWidth(num width );external
           RaphaelContext glow();external

	RaphaelContext setFont(String family , num size , num weight );external
	RaphaelContext setRawFont(String font );external
	RaphaelContext setFillStyle(String style );external
	RaphaelContext setBackgroundFillStyle(String style );external
	RaphaelContext setStrokeStyle(String style );external
	RaphaelContext setShadowColor(String style );external
	RaphaelContext setShadowBlur(String blur );external
	RaphaelContext setLineDash(String dash );external
	RaphaelContext setLineCap(String cap_type );external
	RaphaelContext scale(num x , num y );external
	void clear();external
	RaphaelContext resize(num width , num height );external
	void setViewBox(String viewBox );external
	void rect(num x , num y , num width , num height );external
	RaphaelContext fillRect(num x , num y , num width , num height );external
	RaphaelContext clearRect(num x , num y , num width , num height );external
	RaphaelContext beginPath();external
	RaphaelContext moveTo(num x , num y );external
	RaphaelContext lineTo(num x , num y );external
	RaphaelContext bezierCurveToTo(num x1 , num y1 , num x2 , num y2 , num x , num y );external
	RaphaelContext quadraticCurveToTo(num x1 , num y1 , num x , num y );external
	RaphaelContext arc(num x , num y , num radius , num startAngle , num endAngle , bool antiClockwise );external
	RaphaelContext fill();external
	RaphaelContext stroke();external
	RaphaelContext closePath();external
	dynamic measureText(String text );external
	RaphaelContext fillText(String text , num x , num y );external
	RaphaelContext save();external
	RaphaelContext restore();
}

@JS()
class Renderer   {
	static  bool USE_CANVAS_PROXY;
	external factory Renderer ([ html.Element sel, int backend ]);
	external static IRenderContext buildContext(html.Element sel , int backend, num width , num height , String background );
	external static CanvasContext getCanvasContext(html.Element sel , int backend, num width , num height , String background );
	external static RaphaelContext getRaphaelContext(html.Element sel , int backend, num width , num height , String background );
	external static SVGContext getSVGContext(html.Element sel , int backend, num width , num height , String background );
	external static CanvasContext bolsterCanvasContext(CanvasRenderingContext2D ctx );
	external static void drawDashedLine(IRenderContext context , num fromX , num fromY , num toX , num toY , List<num> dashPattern );external
	Renderer resize(num width , num height );external
	IRenderContext getContext();
}

class RendererBackends {
	static const int CANVAS = 1;
	static const int RAPHAEL = 2;
	static const int SVG = 3;
	static const int VML = 4;
}

class RendererLineEndType {
	static const int NONE = 1;
	static const int UP = 2;
	static const int DOWN = 3;
}

@JS()
class Repetition  extends StaveModifier  {
	external factory Repetition ([ Repetition.type type, num x, num y_shift ]);
	external String getCategory();external
	Repetition setShiftX(num x );external
	Repetition setShiftY(num y );external
	Repetition draw(Stave stave , num x );external
	Repetition drawCodaFixed(Stave stave , num x );external
	Repetition drawSignoFixed(Stave stave , num x );external
	Repetition drawSymbolText(Stave stave , num x , String text , bool draw_coda );
}

class RepetitionType {
    static const int NONE = 1;
		static const int CODA_LEFT = 2;
		static const int CODA_RIGHT = 3;
		static const int SEGNO_LEFT = 4;
		static const int SEGNO_RIGHT = 5;
		static const int DC = 6;
		static const int DC_AL_CODA = 7;
		static const int DC_AL_FINE = 8;
		static const int DS = 9;
		static const int DS_AL_CODA = 10;
		static const int DS_AL_FINE = 11;
		static const int FINE = 12;
}

@JS()
class Stave   {
	external factory Stave ([ num x, num y, num width, dynamic options ]);
	external void resetLines();external
	Stave setNoteStartX(num x );external
	num getNoteStartX();external
	num getNoteEndX();external
	num getTieStartX();external
	num getTieEndX();external
	Stave setContext(IRenderContext context );external
	IRenderContext getContext();external
	num getX();external
	num getNumLines();external
	Stave setY(num y );external
	Stave setWidth(num width );external
	num getWidth();external
	Stave setMeasure(num measure );external
	Stave setBegBarType(int type);external
	Stave setEndBarType(int type);external
	num getModifierXShift(num index );external
	Stave setRepetitionTypeLeft(int type, num y );external
	Stave setRepetitionTypeRight(int type, num y );external
	Stave setVoltaType(int type, num num_t , num y );external
	Stave setSection(String section , num y );external
	Stave setTempo(dynamic tempo , num y );external
	Stave setText(String text , int position, dynamic options );external
	num getHeight();external
	num getSpacingBetweenLines();external
	BoundingBox getBoundingBox();external
	num getBottomY();external
	num getBottomLineY();external
	num getYForLine(num line );external
	num getYForTopText(num line );external
	num getYForBottomText(num line );external
	num getYForNote(num line );external
	num getYForGlyphs();external
	Stave addGlyph(Glyph glypg );external
	Stave addEndGlyph(Glyph glypg );external
	Stave addModifier(StaveModifier modifier );external
	Stave addEndModifier(StaveModifier modifier );external
	Stave addKeySignature(String keySpec );external
	Stave addClef(String clef ,[String size , String annotation]);external
	Stave addEndClef(String clef,[String size , String annotation]);external
	void addTimeSignature(String timeSpec , num customPadding );external
	Stave addTrebleGlyph();external
	void draw();external
	void drawVertical(num x , bool isDouble );external
	void drawVerticalFixed(num x , bool isDouble );external
	void drawVerticalBar(num x );external
	void drawVerticalBarFixed(num x );external
	List<dynamic> getConfigForLines();external
	Stave setConfigForLine(num line_num , dynamic line_config );external
	Stave setConfigForLines(List<dynamic> lines_configuration );
}

@JS()
class StaveConnector   {
	static const int SINGLE_RIGHT = 0;
	static const int SINGLE_LEFT = 1;
	static const int SINGLE = 1;
	static const int DOUBLE = 2;
	static const int BRACE = 3;
	static const int BRACKET = 4;
	static const int BOLD_DOUBLE_LEFT = 5;
	static const int BOLD_DOUBLE_RIGHT = 6;
	static const int THIN_DOUBLE = 7;

	external factory StaveConnector ([ Stave top_stave, Stave bottom_stave ]);
	external StaveConnector setContext(IRenderContext ctx );external
	StaveConnector setType(int type);external
	StaveConnector setText(String text , dynamic text_options );external
	void setFont(dynamic font );external
	StaveConnector setXShift(num x_shift );external
	void draw();external
	void drawBoldDoubleLine(Object ctx ,int type, num topX , num topY , num botY );
}

@JS()
class StaveHairpin   {
	static const int CRESC = 1;
 	static const int DECRESC = 2;

	external factory StaveHairpin ([ dynamic notes, StaveHairpin.type type ]);
	external static void FormatByTicksAndDraw(IRenderContext ctx , Formatter formatter , dynamic notes , int type, int position, dynamic options );external
	StaveHairpin setContext(IRenderContext context );external
	StaveHairpin setPosition(int position);external
	StaveHairpin setRenderOptions(dynamic options );external
	StaveHairpin setNotes(dynamic notes );external
	void renderHairpin(dynamic params );external
	bool draw();
}

@JS()
class StaveLine   {
	external factory StaveLine ([ dynamic notes ]);
	external StaveLine setContext(Object context );external
	StaveLine setFont(dynamic font );external
	StaveLine setText(String text );external
	StaveLine setNotes(dynamic notes );external
	void applyLineStyle();external
	void applyFontStyle();external
	StaveLine draw();
	external dynamic get render_options;
	external set render_options(dynamic v);
}

class TextVerticalPosition {
	static const int TOP = 1;
	static const int BOTTOM = 2;
}

class TextJustification {
	static const int LEFT = 1;
	static const int CENTER = 2;
	static const int RIGHT = 3;
}


@JS()
class StaveModifier   {
	external factory StaveModifier();
	external String getCategory();external
	dynamic makeSpacer(num padding );external
	void placeGlyphOnLine(Glyph glyph , Stave stave , num line );external
	void setPadding(num padding );external
	StaveModifier addToStave(Stave stave , bool firstGlyph );external
	StaveModifier addToStaveEnd(Stave stave , bool firstGlyph );external
	void addModifier();external
	void addEndModifier();
}

@JS()
@anonymous
class StaveNoteOption {
	external List<String> get keys;
  external String get duration;
	external int get stem_direction;
	external num get glyph_font_scale;
	external int get stroke_px;
	external String get clef;
	external int get octave_shift;
  external factory StaveNoteOption({
		List<String> keys,
		String duration,
		int stem_direction,
		num glyph_font_scale,
		int stroke_px,
		String clef,
		int octave_shift
	});
}

@JS()
class StaveNote  extends StemmableNote  {
	static const int STEM_UP = 1;
	static const int STEM_DOWN = -1;
	String CATEGORY;
	external factory StaveNote ([ StaveNoteOption note_struct ]);
	external StemmableNote buildStem();external
	Note setStave(Stave stave );external
	Note addModifier(Modifier modifier , num index );external
	dynamic getModifierStartXY();external
	num getDots();
	external static bool get DEBUG;
	external static bool format(List<StaveNote> notes , dynamic state );
	external static void formatByY(List<StaveNote> notes , dynamic state );
	external static bool postFormat2(List<StaveNote> notes );external
	void buildNoteHeads();external
	void autoStem();external
	void calculateKeyProps();external
	BoundingBox getBoundingBox();external
	num getLineNumber();external
	bool isRest();external
	bool isChord();external
	bool hasStem();external
	num getYForTopText(num text_line );external
	num getYForBottomText(num text_line );external
	List<String> getKeys();external
	List<dynamic> getKeyProps();external
	bool isDisplaced();external
	StaveNote setNoteDisplaced(bool displaced );external
	num getTieRightX();external
	num getTieLeftX();external
	num getLineForRest();external
	void setStyle(dynamic style );external
	void setFlagStyle(dynamic style );external
	StaveNote setKeyStyle(num index , dynamic style );external
	StaveNote setKeyLine(num index , num line );external
	num getKeyLine(num index );external
	StaveNote addToModifierContext(ModifierContext mContext );external
	StaveNote addAccidental(num index , Accidental accidental );external
	StaveNote addArticulation(num index , Articulation articulation );external
	StaveNote addAnnotation(num index , Annotation annotation );external
	StaveNote addDot(num index );external
	StaveNote addDotToAll();external
	List<Accidental> getAccidentals();external
	num getVoiceShiftWidth();external
	void calcExtraPx();external
	void preFormat();external
	dynamic getNoteHeadBounds();external
	num getNoteHeadBeginX();external
	num getNoteHeadEndX();external
	void drawLedgerLines();external
	void drawModifiers();external
	void drawFlag();external
	void drawNoteHeads();external
	void drawStem(dynamic struct );external
	void draw();
}

@JS()
class StaveSection  extends Modifier  {
	external factory StaveSection ([ String section, num x, num shift_y ]);
	external void draw();external
	String getCategory();external
	StaveSection setStaveSection(String section );external
	StaveSection setShiftX(num x );external
	StaveSection setShiftY(num y );external
	StaveSection draw2(Stave stave , num shift_x );
}

@JS()
class StaveTempo  extends StaveModifier  {
	external factory StaveTempo ([ dynamic tempo, num x, num shift_y ]);
	external String getCategory();external
	StaveTempo setTempo(dynamic tempo );external
	StaveTempo setShiftX(num x );external
	StaveTempo setShiftY(num y );external
	StaveTempo draw(Stave stave , num shift_x );
}

@JS()
class StaveText  extends Modifier  {
	external factory StaveText ([ String text, int position, dynamic options ]);
	external void draw();external
	String getCategory();external
	StaveText setStaveText(String text );external
	StaveText setShiftX(num x );external
	StaveText setShiftY(num y );external
	void setFont(dynamic font );external
	void setText(String text );external
	StaveText draw2(Stave stave );
}

@JS()
@anonymous
class StaveTieParams {
	external Note get first_note;
	external Note get last_notes;
	external List<int> get first_indices;
	external List<int> get last_indices;
	external factory StaveTieParams(
		{Note first_note, Note last_note,
		 List<int> first_indices, List<int> last_indices});
}

@JS()
class StaveTie   {
	external factory StaveTie ([ StaveTieParams params, String text ]);
	external StaveTie setContext(IRenderContext context );external
	StaveTie setFont(dynamic font );external
	StaveTie setNotes(dynamic notes );external
	bool isPartial();external
	void renderTie(dynamic params );external
	void renderText(num first_x_px , num last_x_px );external
	bool draw();
}

@JS()
class Stem   {
	static const int UP = 1;
	static const int DOWN = 2;
	external factory Stem ([ dynamic options ]);
	external static bool get DEBUG;external
	Stem setNoteHeadXBounds(num x_begin , num x_end );external
	void setDirection(num direction );external
	void setExtension(num extension );external
	void setYBounds(num y_top , num y_bottom );external
	String getCategory();external
	Stem setContext(IRenderContext context );external
	num getHeight();external
	BoundingBox getBoundingBox();external
	dynamic getExtents();external
	void setStyle(dynamic style );external
	dynamic getStyle();external
	Stem applyStyle(IRenderContext context );external
	void draw();
	external bool get hide;
	external set hide(bool v);
}

@JS()
class StemmableNote  extends Note  {
	external factory StemmableNote ([ dynamic note_struct ]);
	external Note setBeam();
	external static bool get DEBUG;external
	Stem getStem();external
	StemmableNote setStem(Stem stem );external
	StemmableNote buildStem();external
	num getStemLength();external
	num getBeamCount();external
	num getStemMinumumLength();external
	num getStemDirection();external
	StemmableNote setStemDirection(num direction );external
	num getStemX();external
	num getCenterGlyphX();external
	num getStemExtension();external
	num setStemLength();external
	dynamic getStemExtents();external
	num getYForTopText(num text_line );external
	num getYForBottomText(num text_line );external
	StemmableNote postFormat();external
	void drawStem(dynamic stem_struct );
}

@JS()
class StringNumber  extends Modifier  {
	String CATEGORY;
	external factory StringNumber ([ num num ]);
	external StringNumber setNote(Note note );
	external static bool format(List<StringNumber> nums , dynamic state );external
	Note getNote();external
	num getIndex();external
	StringNumber setIndex(num index );external
	StringNumber setLineEndType(int leg);
	int getPosition();external
	StringNumber setPosition(int position);external
	StringNumber setStringNumber(num num );external
	StringNumber setOffsetX(num x );external
	StringNumber setOffsetY(num y );external
	StringNumber setLastNote(StemmableNote note );external
	StringNumber setDashed(bool dashed );external
	void draw();
}

@JS()
class Stroke  extends Modifier  {
	String CATEGORY ;
	external factory Stroke ([ Stroke.Type type, dynamic options ]);
	external static bool format(List<Stroke> strokes , dynamic state );
	int getPosition();external
	Stroke addEndNote(Note note );external
	void draw();
}

class StrokeType {
	static const int BRUSH_DOWN = 1;
	static const int BRUSH_UP = 2;
	static const int ROLL_DOWN = 3;
	static const int ROLL_UP = 4;
	static const int RASQUEDO_DOWN = 5;
	static const int RASQUEDO_UP = 6;
}

@JS()
class SVGContext    {
	external factory SVGContext ([ html.Element element ]);
	external bool iePolyfill();external
	SVGContext setFont(String family , num size , num weight );external
	SVGContext setRawFont(String font );external
	SVGContext setFillStyle(String style );external
	SVGContext setBackgroundFillStyle(String style );external
	SVGContext setStrokeStyle(String style );external
	SVGContext setShadowColor(String style );external
	SVGContext setShadowBlur(String blur );external
	SVGContext setLineWidth(num width );external
	SVGContext setLineDash(String dash );external
	SVGContext setLineCap(String cap_type );external
	SVGContext resize(num width , num height );external
	SVGContext scale(num x , num y );external
	void setViewBox(num xMin , num yMin , num width , num height );external
	void clear();external
	SVGContext rect(num x , num y , num width , num height );external
	SVGContext fillRect(num x , num y , num width , num height );external
	SVGContext clearRect(num x , num y , num width , num height );external
	SVGContext beginPath();external
	SVGContext moveTo(num x , num y );external
	SVGContext lineTo(num x , num y );external
	SVGContext bezierCurveToTo(num x1 , num y1 , num x2 , num y2 , num x , num y );external
	SVGContext quadraticCurveToTo(num x1 , num y1 , num x , num y );external
	SVGContext arc(num x , num y , num radius , num startAngle , num endAngle , bool antiClockwise );external
	SVGContext closePath();external
	SVGContext glow();external
	SVGContext fill();external
	SVGContext stroke();external
	dynamic measureText(String text );external
	dynamic ieMeasureTextFix(dynamic bbox , String text );external
	SVGContext fillText(String text , num x , num y );external
	SVGContext save();external
	SVGContext restore();
}

@JS()
class  TabNote extends StemmableNote {
	external Note setStave(Stave stave ) ;
  external dynamic getModifierStartXY();
	external factory TabNote(List<dynamic> tab_struct , String type , num dots , String duration , var stem_direction, var draw_stem);
	external String getCategory() ;
	external TabNote setGhost(bool ghost ) ;
	external bool hasStem() ;
	external num getStemExtension() ;
	external TabNote addDot() ;
	external void updateWidth() ;
	external List<dynamic> getPositions();
	external TabNote addToModifierContext(ModifierContext mc ) ;
	external num getTieRightX() ;
	external num getTieLeftX() ;
	external dynamic getModifierStartXY2(int pos, num index );
	external num getLineForRest() ;
	external void preFormat() ;
	external num getStemX() ;
	external num getStemY() ;
	external dynamic getStemExtents();
	external void drawFlag() ;
	external void drawModifiers() ;
	external void drawStemThrough() ;
	external void draw() ;
}

@JS()
class TabSlide  extends TabTie  {
	external factory TabSlide ([ dynamic notes, num direction ]);
	external static TabSlide createSlideUp(dynamic notes );
	external static TabSlide createSlideDown(dynamic notes );external
	void renderTie(dynamic params );
}

class TabSlideType {
	static const int SLIDE_UP = 1;
	static const int SLIDE_DOWN = 2;
}

@JS()
class TabStave  extends Stave  {
	external factory TabStave ([ num x, num y, num width, dynamic options ]);
	external num getYForGlyphs();external
	TabStave addTabGlyph();
}

@JS()
class TabTie  extends StaveTie  {
	external factory TabTie ([ dynamic notes, String text ]);
	external TabTie createHammeron(dynamic notes );external
	TabTie createPulloff(dynamic notes );external
	bool draw();
}

@JS()
class TextBracket   {
	external factory TextBracket ([ dynamic bracket_data ]);
	external static bool get DEBUG;external
	TextBracket applyStyle(IRenderContext context );external
	TextBracket setDashed(bool dashed , List<num> dash );external
	TextBracket setFont(dynamic font );external
	TextBracket setContext(IRenderContext context );external
	TextBracket setLine(num line );external
	void draw();
}

class TextBracketPositions {
	static const int TOP = 1;
	static const int BOTTOM = -1;
}

@JS()
class TextDynamics  extends Note  {
	external factory TextDynamics ([ dynamic text_struct ]);
	external static bool get DEBUG;external
	TextDynamics setLine(num line );external
	TextDynamics preFormat();external
	void draw();
}

@JS()
class  TextNote extends Note {
	dynamic GLYPHS;
	external factory TextNote(dynamic text_struct , num line , bool smooth , ignore_ticks);
	external TextNote setJustification(TextNoteJustification just) ;
	external TextNote setLine(num line ) ;
	external void preFormat() ;
	external void draw() ;
}

class TextNoteJustification {
	static const int LEFT = 1;
	static const int CENTER = 2;
	static const int RIGHT = 3;
}

@JS()
class Tickable extends ElementBase  {
	external factory Tickable();
	external void setContext(IRenderContext context );external
	BoundingBox getBoundingBox();external
	Fraction getTicks();external
	bool shouldIgnoreTicks();external
	num getWidth();external
	Tickable setXShift(num x );external
	num getCenterXShift();external
	bool isCenterAligned();external
	Tickable setCenterAlignment(bool align_center );external
	Voice getVoice();external
	void setVoice(Voice voice );external
	Tuplet getTuplet();external
	Tickable setTuplet(Tuplet tuplet );external
	void addToModifierContext(ModifierContext mc );external
	Tickable addModifier(Modifier mod );external
	void setTickContext(TickContext tc );external
	void preFormat();external
	Tickable postFormat();external
	Fraction getIntrinsicTicks();external
	void setIntrinsicTicks(Fraction intrinsicTicks );external
	Fraction getTickMultiplier();external
	void applyTickMultiplier(num numerator , num denominator );external
	void setDuration(Fraction duration );
}

@JS()
class TickContext   {
	external factory TickContext();
	external void setContext(IRenderContext context );external
	IRenderContext getContext();external
	bool shouldIgnoreTicks();external
	num getWidth();external
	num getX();external
	TickContext setX(num x );external
	num getPixelsUsed();external
	TickContext setPixelsUsed(num pixelsUsed );external
	TickContext setPadding(num padding );external
	num getMaxTicks();external
	num getMinTicks();external
	List<Tickable> getTickables();external
	List<Tickable> getCenterAlignedTickables();external
	dynamic getMetrics();external
	Fraction getCurrentTick();external
	void setCurrentTick(Fraction tick );external
	dynamic getExtraPx();external
	TickContext addTickable(Tickable tickable );external
	TickContext preFormat();external
	TickContext postFormat();
	external static TickContext getNextContext(TickContext tContext );
}

@JS()
class TimeSignature  extends StaveModifier  {
	dynamic glyphs;
	external factory TimeSignature ([ String timeSpec, num customPadding ]);
	external void addModifier();external
	void addEndModifier();external
	dynamic parseTimeSpec(String timeSpec );external
	Glyph makeTimeSignatureGlyph(List<num> topNums , List<num> botNums );external
	dynamic getTimeSig();external
	void addModifier2(Stave stave );external
	void addEndModifier2(Stave stave );
}


@JS()
class TimeSigNote  extends Note  {
	external factory TimeSigNote ([ String timeSpec, num customPadding ]);
	external Note setStave(Stave stave );external
	void setStav2(Stave stave );external
	BoundingBox getBoundingBox();external
	TimeSigNote addToModifierContext();external
	TimeSigNote preFormat();external
	void draw();
}

@JS()
class Tremolo  extends Modifier  {
	external factory Tremolo ([ num num ]);
	external String getCategory();external
	void draw();
}

@JS()
class Tuning   {
	dynamic names;
	external factory Tuning ([ String tuningString ]);
	external num noteToInteger(String noteString );external
	void setTuning(String tuningString );external
	num getValueForString(String StringNum );external
	num getValueForFret(String fretNum , String StringNum );external
	String getNoteForFret(String fretNum , String StringNum );
}

@JS()
class Tuplet   {
	num LOCATION_TOP ;
	num LOCATION_BOTTOM ;
	external factory Tuplet ([ List<StaveNote> notes, dynamic options ]);
	external void attach();external
	void detach();external
	Tuplet setContext(IRenderContext context );external
	Tuplet setBracketed(bool bracketed );external
	Tuplet setRatioed(bool ratioed );external
	Tuplet setTupletLocation(num location );external
	List<StaveNote> getNotes();external
	num getNoteCount();external
	num getBeatsOccupied();external
	void setBeatsOccupied(num beats );external
	void resolveGlyphs();external
	void draw();
}

@JS()
class Vibrato  extends Modifier  {
	String CATEGORY ;
	external factory Vibrato();
	external static bool format(List<Vibrato> vibratos , dynamic state , ModifierContext context );external
	Vibrato setHarsh(bool harsh );external
	Vibrato setVibratoWidth(num width );external
	void draw();
}

@JS()
@anonymous
class VoiceParams {
	external int get num_beats;
	external int get beat_value;
	external factory VoiceParams({int num_beats, int beat_value});
}

@JS()
class Voice   {
	external factory Voice ([ VoiceParams time ]);
	external Fraction getTotalTicks();external
	Fraction getTicksUsed();external
	num getLargestTickWidth();external
	Fraction getSmallestTickCount();external
	List<Tickable> getTickables();external
	num getMode();external
	Voice setMode(num mode );external
	num getResolutionMultiplier();external
	num getActualResolution();external
	Voice setStave(Stave stave );external
	BoundingBox getBoundingBox();external
	VoiceGroup getVoiceGroup();external
	Voice setVoiceGroup(VoiceGroup g );external
	Voice setStrict(bool strict );external
	bool isComplete();external
	Voice addTickable(Tickable tickable );external
	Voice addTickables(List<Tickable> tickables );external
	Voice preFormat();external
	void draw(IRenderContext context , Stave stave );
}

class VoiceMode {
	static const int STRICT = 1;
	static const int SOFT = 2;
	static const int FULL = 3;
}

@JS()
class VoiceGroup   {
	external factory VoiceGroup();
  external List<Voice> getVoices();external
	List<ModifierContext> getModifierContexts();external
	void addVoice(Voice voice );
}

@JS()
class Volta  extends StaveModifier  {
	external factory Volta ([ Volta.type type, num num, num x, num y_shift ]);
	external String getCategory();external
	Volta setShiftY(num y );external
	Volta draw(Stave stave , num x );
}

class VoltaType {
    static const int NONE = 1;
		static const int BEGIN =2;
		static const int MID = 3;
		static const int END =4;
		static const int BEGIN_END = 5;
}

@JS()
class DraftPitch  extends StaveNote  {
	external factory DraftPitch ([ dynamic note_struct ]);
	external num getStemExtension();external
	String getCategory();external
	void draw();
}
