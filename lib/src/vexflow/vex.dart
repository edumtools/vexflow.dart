@JS('Vex')
library vexflow.vex.impl;

import "package:func/func.dart";
import "package:js/js.dart";
import 'dart:html' as html;
import 'dart:web_audio' show AudioContext;
import 'dart:typed_data';

import 'vexflow.d.dart';

@JS()
external void L(String block , List<dynamic> args ) ;
@JS()
external Object Merge(Object destination , Object source ) ;
@JS()
external num Min(num a , num b ) ;
@JS()
external num Max(num a , num b ) ;
@JS()
external num RoundN(num x , num n ) ;
@JS()
external num MidLine(num a , num b ) ;
//@JS()
//external SortAndUnique<T extends List<dynamic>>(T arr , Function cmp , Function eq ) ;

@JS()
external bool Contains(List<dynamic> a , dynamic obj ) ;
@JS()
external CanvasRenderingContext2D getCanvasContext(String canvas_sel ) ;
@JS()
external void drawDot(IRenderContext ctx , num x , num y , String color ) ;
@JS()
external void BM(num s , Function f ) ;
